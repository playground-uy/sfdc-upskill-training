# Árboles

Un árbol es una estructura que tiene nodos, uno de ellos se considera la raíz o nodo base y cada uno de ellos tiene un conjunto (potencialmente vacio) de nodos hijos (también se le llaman hojas)

C:
|-- Program Files
|    |-- Call of Duty
|    |-- Winamp
|-- Windows

## Ejemplo: Sistema de directorios

```
class Folder {
  constructor(id, files = [], subfolders = []) {
    this.id = id;
    this.files = files;
    this.subfolders = subfolders;
  }

  addSubfolder(folder) {
    this.subfolders.push(folder);
  }
}

class Tree {
  constructor() {
    this.root = new Folder('root');
  }

  /**
  * Adds a node to the tree, using a base - leaf iterative algorithm.
  */
  addNode(parentId, node) {
    let currentNode = this.root;
    let i = 0;
    let found = false;
    while (!found && currentNode.id !== parentId) {
      currentNode = currentNode.subfolders[i];
      i++;
    }
    if (found) ...
    WTF!!
  }


}
```

El problema de agregar nodos a un arbol no se puede resolver con iteraciones sencillas. Necesitamos una de dos herramientas: Recursion o iteracion con memoria.

## Recursión

### Ejemplo: Operación matemática "factorial"

Definicion:
n! = n*(n-1)!, con n > 1;
n! = 1, con n = 1;

En JS

function fact(n) {
  if (n === 1) {
    return 1;
  } else {
    return n*fact(n - 1);
  }
}

### Ejemplo: Sumatoria

function sumatoria(base, n) {
  // paso base
  if (n === 0) {
    return base;
  } else {
    // paso recursivo
    return base + sumatoria(base, n -1);
  }
}
