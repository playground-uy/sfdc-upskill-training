## Ejemplo 2: Carrera de postas o por relevos

Tengo una cantidad de participantes donde cada uno va a tener que pasar su "posta" al siguiente, y solo al siguiente. 

### Estructura

Las limitantes de la realidad nos dan la estructura de lista enlazada (linked list).
Conceptualmente una lista enlazada es una estructura de nodos, donde cada nodo tiene su contenido y un enlace al siguiente nodo.

```
var nodo = {
  elemento: { /* Lo que sea */},
  siguiente: null, // enlace al siguiente nodo
}
```

#### Armado de la lista

```
agregarElemento(nodoActual, nodoSiguiente) {
  nodoActual.siguiente = nodoSiguiente;
}
```

```
var elementos = [{
  nombre: 'J1',
  velocidad: 20,
},
{
  nombre: 'J2',
  velocidad: 20,
},{
  nombre: 'J3',
  velocidad: 20,
},{
  nombre: 'J4',
  velocidad: 20,
}];

const lista = {
  elemento: elementos[0],
  siguiente: {
    elemento: elementos[1],
    siguiente: {
      elemento: elementos[2],
      siguiente: {
        elementos[3],
        siguiente: null
      }
    }
  }
}
```

#### Armado de la lista basada en clases

````
class ListNode {
  constructor(element: object) {
    this.element = element;
    this.next = null;
  }

  function hasNext() {
    this.next !== null;
  }
}

class LinkedList {
  constructor() {
    this.firstNode = null;
  }

  addNode(newNode: ListNode) {
    // if the first node is null, I need to set this one as the first one.
    if (this.firstNode === null) {
      this.firstNode = newNode;
    } else {
      // need to insert at the end of the list;
      let node = this.firstNode; // start from the first node.
      while (node.next !== null) {
        // the current node is not the last one, so I check the next.
        node = node.next;
      }
      // when I reach here, the "node" variable contains the last element in the list.
      node.next = newNode;
    }
  }

  addNodeFirst(newNode: ListNode) {
    if (this.firstNode !== null) {
      // make the current first node as the next to the new node.
      newNode.next = this.firstNode;
    }
    // make the new node be the first node.
    this.firstNode = newNode;
  }

  deleteNode(node: ListNode) {
    if (this.firstNode === node) {
      this.firstNode = this.firstNode.next;
    } else {
      let currentNode = this.firstNode;
      // see if the next node is the one I'm looking for. Otherwise, just advance.
      while (currentNode.next !== node) {
        currentNode = currentNode.next;
      }
      // if I'm here, currentNode.next is the node I'm looking for.
      let nextNode = currentNode.next;
      currentNode.next = nextNode.next;
      // cleanup references from the deleted element.
      nextNode.next = null;
    }
  }

  /**
  * Returns a linked list of elements in reverse order than the current list.
  */
  reverse() {
    let reversedList = new LinkedList();
    
    // base
    let newFirstNode = new ListNode(this.firstNode.element);
    let currentNode = this.firstNode;

    while (currentNode !== null && currentNode.hasNext()) {
      // new node with the original list's next node data.
      let helperNode = new ListNode(currentNode.next.element);
      // make new node point to the current first node.
      helperNode.next = newFirstNode;
      // the new first node is the latest one.
      newFirstNode = helperNode;

      currentNode = currentNode.next;
    }
    // set the latest first as the reversed list first node.
    reversedList.firstNode = newFirstNode;

    return reversedList;
  }
}

```