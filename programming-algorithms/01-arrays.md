# Estructuras y Algoritmos

## Background

En javascript (asi como en casi todos los lenguajes) existen los tipos básicos de datos (string, boolean, number, object, array, function). 

Para resolver problemas complejos suele no ser suficiente con ellos y tenemos que pasar a modelar estructuras y algoritmos para manejarlas. 

## Ejemplo: Lista de contactos

En una lista de contactos, necesito almacenar la información de cada contacto, necesito poder buscar y encontrar un contacto en particular. Editar, agregar, remover suelen ser operaciones necesarias. 

### Estructura

Si asumimos que ningún dato en particular identifica al contacto, la estructura de datos ideal es: **Un array de objetos**

> Nota: También puede ser un mapa de objetos, que en javascript se traduce a un objeto con atributos variables.

```
let contactos = [
  {
    nombre: 'un nombre',
    telefono: '39873298471',
    direccion: '32 hsiuhf ijfmsdf m ',
    email: 'adhhg@nsdja.com',
  },
  {
    nombre: 'otro nombre',
    telefono: '32343298471',
    direccion: ',,,frr ijfmsdf m ',
    email: 'adhhg@994dmc.com',
  },
];
```

### Operaciones o problemas a resolver

#### Busqueda de contactos

**Busqueda simple**

```
function buscar(termino, contactos) {
  for (let i = 0; i < contactos.length; i++) {
    let contacto = contactos[i];
    // si el nombre del contacto contiene el término de búsqueda, lo devuelvo.
    if (contacto.nombre.indexOf(termino) >= 0) {
      return contacto;
    }
  }
  // si llega hasta acá, no encontré nada.
  return null;
}
```

**Busqueda total**
```
function buscarTodos(termino, contactos) {
  let result = []; // aca guardo todas las coincidencias.

  for (let i = 0; i < contactos.length; i++) {
    let contacto = contactos[i];
    // si el nombre del contacto contiene el término de búsqueda, lo agrego.
    if (contacto.nombre.indexOf(termino) >= 0) {
      result.push(contacto);
    }
  }

  return result;
}
```

#### Eliminar elementos

**Eliminar coincidencias**

```
function elimnarCoincidencias(termino, contactos) {

  let i = contactos.length -1;
  // recorro la lista de final a principio
  while(i >= 0) {
    let contacto = contactos[i];
    if (contacto.nombre.indexOf(termino) >= 0) {
      contactos.splice(i, 1);
    }
    i--; // en la proxima iteracion voy a mirar al elemento anterior en la lista
  }
  
  return result;
}
```